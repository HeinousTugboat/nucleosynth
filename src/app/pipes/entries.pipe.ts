import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'entries'
})
export class EntriesPipe implements PipeTransform {

  transform(map: Map<any, any>): any[] {
    console.log('entries pipe!', map);
    return [...map.entries()];

  }

}
