export type ElementType = 'ion' | 'isotope' | 'exotic' | 'molecule' | 'subatomic' | 'dark';

export interface ElementDictionary {
    [K: string]: number;
}

export interface DecayTypeJSON {
    ratio: number;
    reaction: {
        reactant: ElementDictionary,
        product: ElementDictionary
    };
}

export const enum DecayTypes {
    'beta-' = 'beta-',
    '2beta-' = '2beta-',
    'beta+' = 'beta+',
    '2beta+' = '2beta+',
    'electron_capture' = 'electron_capture',
    'alpha' = 'alpha',
    'beta+p' = 'beta+p',
    'beta-n' = 'beta-n',
    'SF' = 'SF'
}

export interface ResourceDecayJSON {
    half_life: number;
    decay_types: {
        [key in DecayTypes]?: DecayTypeJSON
    };
    binding_energy: number;
}

export interface ResourceJSON {
    energy: number;
    binding_energy: number;
    elements: ElementDictionary;
    anti_particle?: string;
    html: string;
    charge?: number; // Ion only?
    type: ElementType[];
    decay?: ResourceDecayJSON;
}

export interface ResourceTree {
    [K: string]: ResourceJSON;
}
