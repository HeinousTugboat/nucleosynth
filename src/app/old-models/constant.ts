export interface ConstantData {
    'EXOTIC_POWER': number;
    'EXOTIC_START': number;
    'EXOTIC_STEP': number;
    'DARK_POWER': number;
    'DARK_START': number;
    'DARK_STEP': number;
    'REACT_PRICE_INCREASE': number;
    'REACT_POWER_INCREASE': number;
    'GENERATOR_PRICE_INCREASE': number;
    'INFUSE_POWER': number;
    'ELEMENT_CHANCE_BONUS': number;
    'ELECTRON_CHARGE': number;
    'COULOMB_CONSTANT': number;
    'FERMI_RADIUS': number;
    'JOULE_TO_EV': number;
    'U_TO_EV': number;
    'ELECTRONEGATIVITY_CHANCE': number;
}
