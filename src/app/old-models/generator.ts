
export interface GeneratorJSON {
    name: string;
    price: number;
    price_exp: number;
    power: number;
    deps: string[];
    upgrades: string[];
}

export interface GeneratorData {
    [K: string]: GeneratorJSON;
}
