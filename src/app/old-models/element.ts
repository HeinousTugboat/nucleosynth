export interface ElementJSON {
    name: string;
    isotopes: {
        [K: string]: {
            binding_energy: number;
            ratio: number;

        }
    };
    ionization_energy: number[];
    electron_affinity: number[];
    number: number;
    abundance: number;
    van_der_walls: number;
    electronegativity: number;
    includes: string[];
    exotic: string;
    main: string;
    anions: string[];
    cations: string[];
    negative_factor: number;
    positive_factor: number;
    reactions: string[];
}

export interface ElementData {
    [K: string]: ElementJSON;
}
