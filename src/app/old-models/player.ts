import { Upgrade, UpgradeData } from './upgrade';
// import { FusionData } from './fusion';

export interface ElementSlot {
    element: string;
    generators: { [K: string]: number };
    upgrades: UpgradeData;
    // reactions: any[];
    // redoxes: any[];
}

export class PlayerOptions {
    numberformat = {
        format: 'scientific',
        flavor: 'short',
        maxSmall: '1',
        sigFigs: 4
    };
    buyIndex = 0;
    adjustIndex = 0;
    elementBuyInde = 0;
    hideBought = false;
    sortIndex = 0;
    hideAchievements = false;
}

export class PlayerGlobalUpgrades {
    offlineTime = {
        power: {
            base: 0
        }
    };
    // redoxSlots = 1;
    // redoxBandwidth = 1;
    // reactionSlots = 1;
    // reactionBandwidth = 1;
    // fusionArea = 1;
    // fusionBandwidth = 1;
}

// tslint:disable:no-inferrable-types
export class Player {
    elementsUnlocked = 1;
    version  = '0.0.0';
    offline = 0;
    options: PlayerOptions = new PlayerOptions;
    resources: { [k: string]: { number: number, unlocked: boolean } } = {
        '1H': { number: 10, unlocked: true },
        '2H': { number: 0, unlocked: true },
        '3H': { number: 0, unlocked: true },
        'xH': { number: 0, unlocked: true },
        dark_matter: { number: 0, unlocked: true }
    };
    elements: { [k: string]: boolean } = {
        'H': true
    };

    globalUpgrades = new PlayerGlobalUpgrades;

    // exoticUpgrades = {
    //     'H': {
    //         'x-3': false,
    //         'boosts-1': false,
    //         'isotopes-2': false,
    //         'readioactive-2': false,
    //         'ions-2': false,
    //         'ions-charge': false,
    //         'molecules-2': false,
    //         'molecules-size': false,
    //         'x-4': false,
    //         'x-5': false,
    //         'x-6': false,
    //         'x-7': false,
    //         'x-8': false,
    //         'x-9': false,
    //         'boosts-2': false,
    //         'boosts-3': false,
    //         'boosts-4': false,
    //         'synergy-1': false,
    //         'synergy-2': false,
    //         'synergy-3': false,
    //         'synergy-4': false,
    //         'synergy-5': false,
    //         'synergy-6': false,
    //         'synergy-7': false,
    //         'synergy-8': false,
    //         'synergy-9': false,
    //         'synergy-10': false
    //     }
    // };
    // darkUpgrades = {
    //     extraElementSlot1: false,
    //     extraElementSlot2: false,
    //     extraElementSlot3: false,
    //     automation1: false,
    //     automation2: false,
    //     automation3: false
    // };

    // achievements: { [k: string]: number } = {};
    unlocks = {
        exoticGate: false,
        darkGate: false,
        ion: false,
        reactionGate: false,
        fusionGate: false,
        tableGate: false
    };
    elementSlots: ElementSlot[] = [
        {
            upgrades: {},
            generators: {
                '1': 0,
                '2': 0,
                '3': 0,
                '4': 0,
                '5': 0,
                '6': 0,
                '7': 0,
                '8': 0,
                '9': 0,
                '10': 0
            },
            element: 'H'
        }
    ];
    // fusion = new FusionData;
    statistics = {
        exoticRun: {
            'H': {
                '1H': 0
            }
        },
        darkRun: {},
        allTime: {}
    };

    lastLogin: number = 0; // TODO: Convert to Date instead of number.

    constructor() {
    }
}
