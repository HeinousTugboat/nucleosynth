import { ElementSlot, Player } from './player';

export interface IUpgrade {
    name: string;
    checkUpgrade(): boolean;
    upgrade(): IUpgrade;
    fn?(): void;
    cost(): number;
}

export interface Upgrade {
    name: string;
    price: number;
    deps: string[];
    exotic_deps?: string[];
    dark_deps?: string[];
    tags: string[];
    tiers: string[];
    function: ((data: ProductionData) => void) | ((data: PlayerData) => void);
    priceFn: (tier: number) => number;
}

export interface ProductionData {
    production: number;
    slot: ElementSlot;
}

export interface PlayerData {
    player: Player;
}

export interface UpgradeData {
    [K: string]: Upgrade;
}

export const enum PowerType {
    base = 'base',
    linear = 'linear',
    exp = 'exp',
    poly = 'poly'
}

export interface GlobalUpgrade {
    price: {[K: string]: number};
    name: string;
    description: string;
    tiers: string[];
    deps: string[];
    exotic_deps: string[];
    tags: string[];
    power: {
        [key in PowerType]?: number;
    };
    price_exp: number;
    repeatable: boolean;
    adjustable?: boolean;
}

export interface GlobalUpgradeData {
    [K: string]: GlobalUpgrade;
}

export class ElementMultiplier implements Upgrade {
    static prices = {
        1: { 1: 1.00e2, 2: 1.00e3, 3: 1.00e4, 4: 1.00e5, 5: 1.00e6 },
        2: { 1: 1.00e7, 2: 1.50e8, 3: 2.25e9, 4: 3.38e10, 5: 5.06e11 },
        3: { 1: 7.59e12, 2: 1.01e14, 3: 1.35e15, 4: 1.80e16, 5: 2.40e17 },
        4: { 1: 3.20e18, 2: 4.00e19, 3: 5.00e20, 4: 6.25e21, 5: 7.81e22 },
        5: { 1: 9.77e23, 2: 1.17e25, 3: 1.41e26, 4: 1.69e27, 5: 2.03e28 },
        6: { 1: 2.43e29, 2: 2.84e30, 3: 3.31e31, 4: 3.86e32, 5: 4.50e33 },
        7: { 1: 5.25e34, 2: 6.00e35, 3: 6.86e36, 4: 7.84e37, 5: 8.96e38 },
        8: { 1: 1.02e40, 2: 1.15e41, 3: 1.3e42, 4: 1.46e43, 5: 1.64e44 }
    };

    name = '';
    get price() {
        return this.priceFn(1);
    }
    quantity = 0;
    deps: string[] = [];
    exoticDeps: string[] = [];
    darkDeps: string[] = [];
    tags: string[] = [];
    tiers: string[] = [];
    function: ((data: ProductionData) => void) | ((data: PlayerData) => void);

    static price(tier: number, gen: number, n: number) {
        if (tier === 1) {
            return Math.pow(10, tier + n + gen - 1);
        }
        return Math.pow(10, tier + n + gen - 1) *
            Math.pow(1e4, tier - 1) *
            Math.pow((tier + 1) / tier, n - 1) *
            Math.pow(tier / 2, 5);
    }

    static func = (level: number) => (x: ProductionData) => x.production *= (level + 1);
    constructor(public level: number) {
        this.function = ElementMultiplier.func(level);
    }

    priceFn(tier: number): number {
        return ElementMultiplier.price(tier, this.level, this.quantity + 1);
    }

}

export class ExoticMultiplier implements Upgrade {
    name = 'Extra Prod';
    price = 0;
    deps = [];
    exotic_deps = [];
    dark_deps = [];
    tags = [];
    tiers = [];
    function() {}
    priceFn() { return 0; }
}
