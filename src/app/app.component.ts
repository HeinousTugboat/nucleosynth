import { Component } from '@angular/core';
import { environment } from '@env';
import { CoreLoopService } from '@services/core-loop.service';
import { DataService } from '@services/data.service';
import { GeneratorService } from '@services/generator.service';
import { SaveService } from '@services/save.service';
import { StateService } from '@services/state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Nucleosynth';
  version = environment.VERSION;

  constructor(
    public coreLoop: CoreLoopService,
    public state: StateService,
    public data: DataService,
    public generators: GeneratorService,
    private save: SaveService
  ) { }
}
