import { Component, OnInit } from '@angular/core';
import { StateService } from '@services/state.service';

@Component({
  selector: 'app-generator-view',
  templateUrl: './generator-view.component.html',
  styleUrls: ['./generator-view.component.scss']
})
export class GeneratorViewComponent implements OnInit {

  constructor(public state: StateService) { }

  ngOnInit() {

  }
}
