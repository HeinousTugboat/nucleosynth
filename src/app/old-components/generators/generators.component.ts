// import { Component, OnInit, Input } from '@angular/core';
// // import { GeneratorService } from '../../controllers/generator.service';
// // import { StateService } from '../../services/state.service';
// import { DataService } from '../../data.service';
// // import { GeneratorData, GeneratorJSON } from '../../models/generator';
// // import { ElementSlot, Player } from '../../models/player';

// @Component({
//   selector: 'app-generators',
//   templateUrl: './generators.component.html',
//   styleUrls: ['./generators.component.scss']
// })
// export class GeneratorsComponent implements OnInit {

//   // @Input()
//   // slot!: ElementSlot;

//   // @Input()
//   // generators!: GeneratorJSON[];
//   // columns = ['genName'];
//   dataColumns = ['genName', 'genProd', 'genNumber', 'genTotalProd', 'genPrice', 'genBuy'];

//   constructor(
//     // public genService: GeneratorService,
//     // public state: StateService,
//     public data: DataService
//   ) { }

//   ngOnInit() {
//     this.slot = this.state.player.elementSlots[0];
//     if (this.slot !== undefined && this.slot.generators !== undefined) {
//       this.generators = this.genService.visibleGenerators(this.slot);
//     }
//   }

//   selectElement(element: string, index: number, player: Player) {
//     const slot = {} as ElementSlot;
//     // tslint:disable-next-line:forin
//     // for (const key in this.data.elementSlot) {
//     //   slot[key] = {...this.data.elementSlot[key]};
//     // }
//     const first = Object.keys(this.data.generators)[0];
//     slot.generators[first] = 1;
//     slot.element = element;
//     player.elementSlots[index] = slot;

//     // const cachedReactions = this.state.reactionsCache[slot.element];
//     // if (cachedReactions) {
//     //   slot.reactions = cachedReactions;
//     // }
//     // const cachedRedoxes = this.state.redoxesCache[slot.element];
//     // if (cachedRedoxes) {
//     //   slot.redoxes = cachedRedoxes;
//     // }
//   }
// }
