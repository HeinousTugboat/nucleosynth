import { Injectable } from '@angular/core';
import { CoreLoopService } from '@services/core-loop.service';
import { DataService } from '@services/data.service';
import { StateService } from '@services/state.service';
import { interval } from 'rxjs';
import { map, switchMap, throttle } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaveService {
  private lastSave = Date.now();
  private lastLoad = Date.now();
  private sessionStart = Date.now();
  private saveKey = 'nucleosynth';

  constructor(
    private coreLoop: CoreLoopService,
    private data: DataService,
    private state: StateService,
  ) {
    this.load();

    const delay$ = data.config$.pipe(
      map(config => config.autosaveDelay),
      switchMap(delay => interval(delay)),
    );

    coreLoop.loop$.pipe(throttle(() => delay$))
      .subscribe(loop => {
        if (loop.debug) { console.log('save.service:', loop.tick); }
        this.save();
      });
  }

  load() {
    this.lastLoad = Date.now();
    const data = localStorage.getItem(this.saveKey);
    if (data === null) {
      this.initialize();
    } else {
      console.log(data);
    }
  }

  save() {
    this.lastSave = Date.now();
    localStorage.setItem(this.saveKey, JSON.stringify({}));
  }

  initialize() {

  }
}
