import { inject, TestBed } from '@angular/core/testing';
import { CoreLoopService } from './core-loop.service';

describe('LoopService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoreLoopService]
    });
  });

  it('should be created', inject([CoreLoopService], (service: CoreLoopService) => {
    expect(service).toBeTruthy();
  }));

  describe('initialization functions', () => {
    xit('should init all the variables');
  });

  describe('update', () => {
    xit('should not update player if nothing is purchased');
    xit('should generate isotopes 1');
    xit('should generate isotopes 2');
    xit('should generate isotopes 3');
    xit('should generate isotopes with very low probability 1');
    xit('should generate isotopes with very low probability 2');
    xit('should process radioactivity');
    xit('should process multi decay');
    xit('should process very high half-lifes');
  });
});
