import { Injectable } from '@angular/core';
import { ElementData, HydrogenData } from '@models/element-data.model';
import { ElementSlot } from '@models/element-slot.model';
import { ParticleGenerator } from '@models/particle-generator.model';
import { Resource } from '@models/resource.model';
import { Upgrade } from '@models/upgrade.model';
import { DataService } from '@services/data.service';

export interface ProductionSlot {
  generators: ParticleGenerator[];
  upgrades: Upgrade[];
  produces: Resource[];
}

/**
 * Handles current state of the game.
 * TODO: Needs this stuff...
 *   * Current element slots
 *   * Current resource counts
 *   * Current upgrades
 *   * Save details..
 *   * Achievements!
 *   * UI state..
 *
 * @export
 * @class StateService
 */
@Injectable({
  providedIn: 'root'
})
export class StateService {
  unlocked: Map<ElementData, boolean> = new Map();
  slots: ElementSlot[] = [];
  resources: Resource[] = []; // Current amounts of resources.
  achievements: any[] = []; // Current achievements?
  uiState: any[] = []; // Any UI specific state..

  constructor(
    private data: DataService
  ) {
    this.initialize();
  }

  initialize() {
    this.achievements = [];
    this.uiState = [];
    this.slots = [];
    Resource.list = new Map();

    const element = new ElementData(HydrogenData);
    this.unlocked.set(element, true);
    const slot = new ElementSlot(element);
    this.slots.push(slot);
    this.resources = [...Resource.list.values()];

    // tslint:disable-next-line:no-non-null-assertion
    // slot.purchase(slot.generators.get('I')!, 1);
  }
}
