import { inject, TestBed } from '@angular/core/testing';
import { StateService } from './state.service';

describe('StateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StateService]
    });
  });

  it('should be created', inject([StateService], (service: StateService) => {
    expect(service).toBeTruthy();
  }));

  describe('toast functions', () => {
    xit('should add toasts');
    xit('should remove toasts');
    xit('should not flip the visibility when removing toasts');
    xit('should not fail on empty toast queues');
    xit('should delete toasts');
    xit('should shift toasts');
    xit('should make upcoing toasts visible');
    xit('should not fail on delete empty lists');
  });

  describe('has new functions', () => {
    xit('should return true if an element has new items');
    xit('should return false if an element has no new items');
    xit('should add and remove items correctly');
    xit('should not remove items that aren\t there');
  });
});
