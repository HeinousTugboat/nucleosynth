import { Injectable } from '@angular/core';
import { CoreLoopService, Loop } from '@services/core-loop.service';
import { DataService } from '@services/data.service';
import { StateService } from '@services/state.service';
import { Resource } from '@models/resource.model';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {
  private darkMatter = Resource.get('dark');

  constructor(
    private data: DataService,
    private state: StateService,
    coreLoop: CoreLoopService
  ) {
    coreLoop.loop$.subscribe(loop => this.update(loop));
  }

  update(loop: Loop): void {
    this.state.slots.forEach((slot) => {
      const x = 1 + (slot.element.exotic.quantity * this.data.constants.exoticPower);
      const d = 1 + (this.darkMatter.quantity * this.data.constants.darkPower);

      // In the original, this is floored before increased.
      slot['resource'].quantity += slot.tick(loop.interval) * x * d;
    });
  }
}
