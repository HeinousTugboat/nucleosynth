import { Injectable } from '@angular/core';
import { DataService } from '@services/data.service';
import { animationFrameScheduler, ConnectableObservable, EMPTY, iif, interval as intervalObservable, Observable, Subject } from 'rxjs';
import { map, publish, switchMap, tap, timeInterval } from 'rxjs/operators';

export interface Loop {
  interval: number;
  value: number;
  tick: number;
  debug: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CoreLoopService {
  public readonly loop$: ConnectableObservable<Loop>;
  private readonly running$: Subject<boolean> = new Subject();
  private readonly loopSource$: Observable<Loop>;
  private _tick = 0;
  private _time = performance.now();
  private debug = false;
  private tickLoops = 1_000;

  constructor(private data: DataService) {
    data.config$.subscribe(config => {
      this.debug = config.debug;
      this.tickLoops = config.tickLoops;
    });

    this.loopSource$ = intervalObservable(0, animationFrameScheduler).pipe(
      timeInterval(animationFrameScheduler),
      map(loop => ({ tick: this._tick, debug: this.debug, ...loop }))
    );

    this.loop$ = this.running$.pipe(
      tap(x => console.log('running$', x)),
      switchMap(active => iif(() => active, this.loopSource$, EMPTY)),
      publish()
    ) as ConnectableObservable<Loop>;

    this.loop$.subscribe(loop => {
      if (loop.debug && this._tick % this.tickLoops === 0) {
        console.log('loop$: ticktock! (' + loop.interval + 'ms) ' + this._tick + ' : ' + (this._tick - loop.value));
      }
      this._time = performance.now();

      ++this._tick;
    });

    this.loop$.connect();
    this.running$.next(true);
  }

  get tick() { return this._tick; }
  get time() { return this._time; }

  pause() { this.running$.next(false); }
  play() { this.running$.next(true); }
}
