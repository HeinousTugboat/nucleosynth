import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface ConfigData {
  debug: boolean;
  autosaveDelay: number;
  saveKey: string;
  tickLoops: number;
}

/**
 * Handles canonical data, loading data, and initial data sets.
 * TODO: Needs this stuff...
 *   * Hydrogen info
 *   * Slot info..
 *   * Upgrade info..
 *   * Baseline Reactions.. Hydrogen/Oxygen?
 * @export
 * @class DataService
 */
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private config: ConfigData = {
    saveKey: 'nucleosynth',
    debug: true,
    autosaveDelay: 60_000,
    tickLoops: 10_000
  };
  private configSource$ = new BehaviorSubject(this.config);
  config$ = this.configSource$.asObservable();
  elements: any[] = []; // Raw element data..
  reactions: any[] = []; // Raw reaction data..
  redoxes: any[] = []; // Raw redox data..
  fusion: any[] = []; // Raw fusion data..
  constants: {[K: string]: any} = {
    exoticPower: 0.001,
    darkPower: 0.01
  };

  constructor() { }

  setConfig<T extends keyof ConfigData>(key: T, value: ConfigData[T]) {
    this.config[key] = value;
    this.configSource$.next(this.config);
  }
}
