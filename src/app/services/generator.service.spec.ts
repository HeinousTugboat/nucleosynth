import { inject, TestBed } from '@angular/core/testing';
import { ElementSlot } from '@models/element-slot.model';
import { empty } from 'rxjs';
import { CoreLoopService, Loop } from './core-loop.service';
import { DataService } from './data.service';
import { GeneratorService } from './generator.service';
import { StateService } from './state.service';
import { HydrogenData, ElementData } from '@models/element-data.model';
import { Resource } from '@models/resource.model';
import { ParticleGenerator } from '@models/particle-generator.model';
// tslint:disable:no-non-null-assertion

class MockDataService {
  public readonly constants = {
    exoticPower: 0.001,
    darkPower: 0.01
  };
}
class MockStateService {
  public element: ElementData = new ElementData(HydrogenData);
  public slots: ElementSlot[] = [];

  constructor() {
    this.slots.push(new ElementSlot(this.element));
  }
}
class MockCoreLoopService {
  public loop$ = empty();
}

describe('GeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GeneratorService,
        { provide: DataService, useClass: MockDataService },
        { provide: StateService, useClass: MockStateService },
        { provide: CoreLoopService, useClass: MockCoreLoopService }
      ]
    });


  });

  it('should be created', inject([GeneratorService], (service: GeneratorService) => {
    expect(service).toBeTruthy();
  }));

  describe('production functions', () => {
    let slots: ElementSlot[];
    let gens: GeneratorService;
    let resource: Resource;
    let exotic: Resource;
    let dark: Resource;

    beforeEach(inject([GeneratorService], (genService: GeneratorService) => {
      gens = genService;
      slots = genService['state'].slots;
      resource = Resource.get('1H');
      resource.quantity = 0;
      exotic = Resource.get('xH');
      exotic.quantity = 0;
      dark = Resource.get('dark');
      dark.quantity = 0;
    }));

    it('should calculate the generator production', () => {
      const slot = slots[0];
      for (const gen of slot.generators.values()) {
        gen['_num'] = 1;
      }

      slot.updateRate();

      resource.quantity = 0;
      gens.update({interval: 1000} as Loop);

      expect(resource.quantity).toBe(2691);
    });

    it('should calculate the generator production with exotic matter: I 1, x2, x3, 3250 xH => 25.5/sec', () => {
      const gen = slots[0].generators.get('I') as ParticleGenerator;
      gen['_num'] = 1;
      gen.upgrades[0].bought = true;
      gen.upgrades[1].bought = true;
      slots[0].updateRate();

      exotic.quantity = 3250;
      resource.quantity = 0;
      gens.update({interval: 1000} as Loop);

      expect(resource.quantity).toBe(25.5);
      // expect(resource.quantity).toBe(25);

    });

    it('should calculate the generator production with dark matter: I 1, x2, x3, 3250 dark => 201/sec', () => {
      const gen = slots[0].generators.get('I') as ParticleGenerator;
      gen['_num'] = 1;
      gen.upgrades[0].bought = true;
      gen.upgrades[1].bought = true;
      slots[0].updateRate();

      dark.quantity = 3250;
      resource.quantity = 0;
      gens.update({ interval: 1000 } as Loop);

      expect(resource.quantity).toBe(201);
    });

    it('should calculate the generator production with exotic and dark matter: I 1, x2, x3, 3250 xH, 3250 dark => 854.25/sec', () => {
      const gen = slots[0].generators.get('I') as ParticleGenerator;
      gen['_num'] = 1;
      gen.upgrades[0].bought = true;
      gen.upgrades[1].bought = true;
      slots[0].updateRate();

      exotic.quantity = 3250;
      dark.quantity = 3250;
      resource.quantity = 0;
      gens.update({ interval: 1000 } as Loop);

      expect(resource.quantity).toBe(854.25);
      // expect(resource.quantity).toBe(854);
    });
  });

  // describe('visibility functions', () => {
  //   it('should show visible generators', inject([GeneratorService], (service: GeneratorService) => {
  //     const gen = service['state'].slots[0].generators.get('I') as ParticleGenerator;
  //     gen['_num'] = 1;
  //   }));
  // });

});
