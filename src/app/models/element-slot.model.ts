import { ElementData } from './element-data.model';
import { ParticleGenerator } from './particle-generator.model';
import { Resource } from './resource.model';
import { BasicUpgrade, Upgrade } from './upgrade.model';

export class ElementSlot {
  public generators: Map<string, ParticleGenerator> = new Map();
  public upgrades: Map<string, Upgrade> = new Map();
  private resource: Resource;
  private production = 0;

  constructor(public readonly element: Readonly<ElementData>) {
    const gens: ParticleGenerator[] = [];
    this.resource = element.main as Resource;
    this.resource.quantity = 10;
    gens.push(new ParticleGenerator('I', 1e1, 1, 1.12, this.resource, []));
    gens.push(new ParticleGenerator('II', 1e2, 5, 1.12, this.resource, [gens[0]]));
    gens.push(new ParticleGenerator('III', 1e3, 10, 1.12, this.resource, [gens[1]]));
    gens.push(new ParticleGenerator('IV', 1e4, 25, 1.12, this.resource, [gens[2]]));
    gens.push(new ParticleGenerator('V', 1e5, 50, 1.13, this.resource, [gens[3]]));
    gens.push(new ParticleGenerator('VI', 1e6, 100, 1.14, this.resource, [gens[4]]));
    gens.push(new ParticleGenerator('VII', 1e7, 250, 1.14, this.resource, [gens[5]]));
    gens.push(new ParticleGenerator('VIII', 1e8, 500, 1.14, this.resource, [gens[6]]));
    gens.push(new ParticleGenerator('IX', 1e9, 750, 1.15, this.resource, [gens[7]]));
    gens.push(new ParticleGenerator('X', 1e10, 1000, 1.15, this.resource, [gens[8]]));

    gens.forEach(gen => {
      this.generators.set(gen.name, gen);
      const basic = new BasicUpgrade(gen, 2, 1e5 * gen.power, [gen]);
      this.upgrades.set(basic.name, basic);
      const basic2 = new BasicUpgrade(gen, 3, 1e7 * gen.power, [gen, basic]);
      this.upgrades.set(basic2.name, basic2);
      // let boost = new BoostUpgrade(gen, 0.05, 10, ???, [gen, xBoost]);
      // let boost2 = new BoostUpgrade(gen, 0.25, 25, ???, [gen, boost, xBoost2]);
      // let boost3 = new BoostUpgrade(gen, 1, 50, ???, [gen, boost2, xBoost3]);
      // let boost4 = new BoostUpgrade(gen, 2.5, 100, ???, [gen, boost3, xBoost4]);
      // gens.forEach(source => {
      //     if (gen !== source) {
      //         // let synergy = new SynergyUpgrade(gen, 10, [xSyn(source)], source);
      //     }
      // });

    });
    this.updateRate();

  }

  tick(dT: number): number {
    return this.production * dT / 1000;
  }

  updateRate() {
    this.production = 0;
    for (const gen of this.generators.values()) {
      this.production += gen.getTotalRate();
    }
  }

  get rate() {
    return this.production;
  }

  purchase(gen: ParticleGenerator, n: number): boolean {
    const success = gen.purchase(n);
    this.updateRate();
    return success;
  }

  visibleGenerators(): ParticleGenerator[] {
    const results: ParticleGenerator[] = [];

    for (const gen of this.generators.values()) {
      if (gen.checkAvailable()) {
        results.push(gen);
      }
    }

    return results;
  }
}
