import { Resource } from './resource.model';

export interface ElementJSON {
  name: string;
  symbol: string;
  isotopes: {[K: string]: number};
  ionization_energy: number[];
  electron_affinity: number[];
  number: number;
  abundance: number;
  van_der_wals: number;
  electronegativity: number;
  exotic: string;
  main: string;
  anions: string[];
  cations: string[];
  negative_factor: number;
  positive_factor: number;
}

export class ElementData {
  public static list: Map<string, ElementData> = new Map();
  public readonly name: string;
  public readonly symbol: string;
  public readonly number: number;
  public readonly ratios: Readonly<Map<Resource, number>>;
  public readonly ionizationEnergy: Readonly<number[]>;
  public readonly electronAffinity: Readonly<number[]>;
  public readonly abundance: number;
  public readonly vanDerWals: number;
  public readonly electronegativity: number;
  public readonly exotic: Readonly<Resource>;
  public readonly main: Readonly<Resource>;
  public readonly anions: Readonly<Resource[]>;
  public readonly cations: Readonly<Resource[]>;
  public readonly negativeFactor: number;
  public readonly positiveFactor: number;
  public readonly resources: Readonly<Resource[]>;

  constructor(data: ElementJSON) {
    this.name = data.name;
    this.symbol = data.symbol;
    this.number = data.number;
    this.ionizationEnergy = data.ionization_energy;
    this.electronAffinity = data.electron_affinity;
    this.abundance = data.abundance;
    this.vanDerWals = data.van_der_wals;
    this.electronegativity = data.electronegativity;
    this.negativeFactor = data.negative_factor;
    this.positiveFactor = data.positive_factor;

    this.ratios = new Map();
    for (const name in data.isotopes) {
      if (data.isotopes.hasOwnProperty(name)) {
        this.ratios.set(Resource.get(name), data.isotopes[name]);
      }
    }
    this.exotic = Resource.get(data.exotic);
    this.main = Resource.get(data.main);
    this.anions = [];
    for (const name of data.anions) {
      this.anions.push(Resource.get(name));
    }

    this.cations = [];
    for (const name of data.cations) {
      this.cations.push(Resource.get(name));
    }

    this.resources = [
      this.main,
      this.exotic,
      ...this.ratios.keys(),
      ...this.cations as Resource[],
      ...this.anions as Resource[]
    ];

    ElementData.list.set(this.name, this);
  }
}

export const HydrogenData: ElementJSON = {
  name: 'Hydrogen',
  symbol: 'H',
  isotopes: {
    '1H': 0.999884,
    '2H': 0.000115,
    '3H': 0.000001
  },
  ionization_energy: [13.5984],
  electron_affinity: [-0.7545],
  number: 1,
  abundance: 0.08999397833,
  van_der_wals: 1.2e-10,
  electronegativity: 2.2,
  exotic: 'xH',
  main: '1H',
  anions: ['H-'],
  cations: ['H+'],
  negative_factor: 31.62277660168381,
  positive_factor: 60.255958607435744
};
