export interface ResourceJSON {
  name: string;
  quantity: number;
}

export class Resource {
  static list: Map<string, Resource> = new Map();
  private constructor(
    public name: string,
    public quantity = 0
  ) {
    Resource.list.set(name, this);
  }

  static fromJSON(json: ResourceJSON): Resource {
    let resource = Resource.list.get(json.name);
    if (resource === undefined) {
      resource = new Resource(json.name, json.quantity);
    } else {
      resource.quantity = json.quantity;
    }

    return resource;
  }

  static get(label: string): Resource {
    let resource = Resource.list.get(label);
    if (resource === undefined) {
      resource = new Resource(label);
    }
    return resource;
  }

  toJSON(): ResourceJSON {
    return {
      name: this.name,
      quantity: this.quantity
    };
  }

}
