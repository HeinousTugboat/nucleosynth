import { Dependency, Purchasable } from './resource.interfaces';
import { Resource } from './resource.model';
import { Upgrade, UpgradeJSON, UpgradeType } from './upgrade.model';

export function isGen(
  g: ParticleGenerator | Upgrade | Purchasable | Dependency
): g is ParticleGenerator {
  return (<ParticleGenerator>g).type === 'generator';
}

export interface ParticleGeneratorJSON {
  type: string;
  upgrades: UpgradeJSON[];
  num: number;
  rate: number;
  available: boolean;
  name: string;
  price: number;
  power: number;
  coeff: number;
  deps: string[];
  resource: string;
}

export class ParticleGenerator implements Dependency, Purchasable {
  public type: 'generator' = 'generator';
  public upgrades: UpgradeType[] = [];
  private _num = 0;
  public rate = 0;
  public available = false;

  constructor(
    public name: string,
    public price: number,
    public power: number,
    public coeff: number,
    public currency: Resource,
    public deps: Dependency[] = [],
  ) {
    if (deps.length === 0) {
      this.available = true;
    }
  }
  static fromJSON(json: ParticleGeneratorJSON) {
    if (!json) {
      return null;
    }

    const gen = new ParticleGenerator(
      json.name,
      json.price,
      json.power,
      json.coeff,
      Resource.list.get(json.resource) as Resource,
      []
    );

    gen._num = json.num;
    gen.type = json.type as 'generator';
    gen.rate = json.rate;
    gen.available = json.available;

    return gen;
  }
  toJSON(): ParticleGeneratorJSON {
    return {
      type: this.type,
      upgrades: [...this.upgrades.map(u => u.toJSON() as UpgradeJSON)],
      num: this._num,
      rate: this.rate,
      available: this.available,
      name: this.name,
      price: this.price,
      power: this.power,
      coeff: this.coeff,
      resource: this.currency.name,
      deps: this.deps.map(gen => gen.name)
    };
  }

  get num(): number {
    return this._num;
  }
  purchase(n: number = 1): boolean {
    if (this.checkAvailable()) {
      const cost = this.getCost(n);

      if (this.currency.quantity >= cost) {
        this.currency.quantity -= cost;
        this._num += n;
        return true;
      }
    }
    return false;
  }
  purchaseMax(): boolean {
    return this.purchase(this.getCostMax().n);

  }
  getValue(rate: number): number {
    return this.getCost() * (1 / this.getBaseRate() + 1 / rate);
  }

  isFulfilled(): boolean {
    return this._num > 0 && this.available;
  }
  /**
   * Checks whether all dependencies are fulfilled.
   */
  checkAvailable(): boolean {
    this.available = this.deps.every((item: Dependency) => item.isFulfilled());
    return this.available;
  }

  getBaseRate(): number {
    return this.upgrades.reduce((rate, upgrade) => {
      return rate * upgrade.bonus();
    }, this.power);
  }

  getTotalRate(): number {
    return Math.floor(this.getBaseRate() * this._num);
  }

  getCostMax(): { n: number, cost: number } {
    const c = this.currency.quantity;
    const r = this.coeff;
    const k = this._num;

    const n = Math.floor(Math.log(c * (r - 1) / (this.price * Math.pow(r, k)) + 1) / Math.log(r));
    const cost = this.getCost(n);
    return { n, cost };
  }
  getCost(n: number = 1): number {

    /* Original Method */
    // n = Math.max(n, 1);
    // let p = 0;
    // for (let i = 0; i < n; ++i) {
    //   p += Math.ceil(Math.pow(this.coeff, this._num + i) * this.price);
    // }

    // return p;

    /* Mathy Method [Discounts bulk purchases because rounding!] */
    // https://blog.kongregate.com/the-math-of-idle-games-part-i/
    const k = this._num;
    const p = this.price * Math.pow(this.coeff, k) * (Math.pow(this.coeff, n) - 1) / (this.coeff - 1);
    // Snips off any partial
    return Math.ceil(Math.round(p * 1e10) / 1e10);

    /* Cost of exactly one. */
    // return Math.ceil(Math.pow(this.coeff, this._num) * this.price);
  }

  updateRate(): number {
    this.rate = this.getTotalRate();
    return this.rate;
  }

  getNextCost(): number {
    return this.getCost();
  }
}

// let gens: Generator[] = [];
//                      Label   Cost  Power  Coeff  Deps
// gens.push(new Generator('I', 1e1, 1, 1.12, []));
// gens.push(new Generator('II', 1e2, 5, 1.12, [gens[0]]));
// gens.push(new Generator('III', 1e3, 10, 1.12, [gens[1]]));
// gens.push(new Generator('IV', 1e4, 25, 1.12, [gens[2]]));
// gens.push(new Generator('V', 1e5, 50, 1.13, [gens[3]]));
// gens.push(new Generator('VI', 1e6, 100, 1.14, [gens[4]]));
// gens.push(new Generator('VII', 1e7, 250, 1.14, [gens[5]]));
// gens.push(new Generator('VIII', 1e8, 500, 1.14, [gens[6]]));
// gens.push(new Generator('IX', 1e9, 750, 1.15, [gens[7]]));
// gens.push(new Generator('X', 1e10, 1000, 1.15, [gens[8]]));

// gens.forEach(gen => {
//     let basic = new BasicUpgrade(gen, 2, 1e5 * gen.power, [gen]);
//     // let basic2 = new BasicUpgrade(gen, 3, ???, [gen, basic, xBasic3x]);
//     // let boost = new BoostUpgrade(gen, 0.05, 10, ???, [gen, xBoost]);
//     // let boost2 = new BoostUpgrade(gen, 0.25, 25, ???, [gen, boost, xBoost2]);
//     // let boost3 = new BoostUpgrade(gen, 1, 50, ???, [gen, boost2, xBoost3]);
//     // let boost4 = new BoostUpgrade(gen, 2.5, 100, ???, [gen, boost3, xBoost4]);
//     gens.forEach(source => {
//         if (gen !== source) {
//             // let synergy = new SynergyUpgrade(gen, 10, [xSyn(source)], source);
//         }
//     })
// });
