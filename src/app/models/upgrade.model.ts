import { ParticleGenerator } from './particle-generator.model';
import { Dependency, Purchasable } from './resource.interfaces';

export function isUpgrade(
  g: ParticleGenerator | Upgrade | Purchasable | Dependency
): g is Upgrade {
  return (<Upgrade>g).type === 'upgrade';
}

export type UpgradeType =
  | Upgrade
  | BasicUpgrade
  | BoostUpgrade
  | SynergyUpgrade;

export interface UpgradeBaseJSON {
  available: boolean;
  bought: boolean;
  name: string;
  target: string;
  price: number;
  deps: string[];
  source: string;
  slot: number;
}

export interface MultiplierJSON extends UpgradeBaseJSON {
  type: 'multiplier';
  mult: number;
}

export interface BoostJSON extends UpgradeBaseJSON {
  type: 'boost';
  mult: number;
  num: number;
}

export interface SynergyJSON extends UpgradeBaseJSON {
  type: 'synergy';
}

export type UpgradeJSON = MultiplierJSON | BoostJSON | SynergyJSON;

/**
 * Upgrades are attached to a specific generator, and simply multiply the
 * generators production by their bonus function. Bonus functions can associate
 * any two generators.
 */
export class Upgrade implements Dependency, Purchasable {
  // static list: Map<string, UpgradeType>[] = [new Map()];
  // static synergies = [] as SynergyJSON[];
  public type: 'upgrade' = 'upgrade';
  public available = false;
  public bought = false;
  constructor(
    public name: string = 'Unnamed Upgrade',
    protected target: ParticleGenerator,
    protected bonusFn: () => number = () => 1,
    protected price: number,
    protected deps: Dependency[],
    protected source: ParticleGenerator = target,
    protected slot: number = 0
  ) {
    target.upgrades.push(this);
    // console.log(Upgrade.list);
    // if (Upgrade.list[slot] === undefined) {
    //   Upgrade.list[slot] = new Map();
    // }
    // Upgrade.list[slot].set(this.name, this);
  }
  // static processSynergies() {
  //   Upgrade.synergies.map(this.fromJSON);
  // }
  static fromJSON(json: UpgradeJSON): Upgrade | null {
    // if (Upgrade.list[json.slot].has(json.name)) {
    //   return Upgrade.list[json.slot].get(json.name) as Upgrade;
    // }
    let upgrade: UpgradeType;
    // FIXME: Please!
    const source = {} as ParticleGenerator;
    const target = {} as ParticleGenerator;
    const deps: Dependency[] = [];
    // const target = ParticleGenerator.list[json.slot].get(json.target) as ParticleGenerator;
    // const source = ParticleGenerator.list[json.slot].get(json.source) as ParticleGenerator;
    // const deps: Dependency[] = json.deps.map(name => {
    //   let gen: Dependency = ParticleGenerator.list[json.slot].get(
    //     name
    //   ) as ParticleGenerator;
    //   if (gen === undefined) {
    //     gen = Upgrade.list[json.slot].get(name) as Dependency;
    //   }
    //   return gen;
    // });
    switch (json.type) {
      case 'multiplier':
        upgrade = new BasicUpgrade(target, json.mult, json.price, deps);
        break;
      case 'boost':
        upgrade = new BoostUpgrade(
          target,
          json.mult,
          json.num,
          json.price,
          deps
        );
        break;
      case 'synergy':
        // if (!source) {
        //   Upgrade.synergies.push(json);
        //   return null;
        // } else {
          upgrade = new SynergyUpgrade(target, json.price, deps, source);
        // }
        break;
      default:
        throw new Error('Unknown type: ' + json);
    }
    upgrade.bought = json.bought;
    return upgrade;
  }
  toJSON(): UpgradeBaseJSON {
    return {
      available: this.available,
      bought: this.bought,
      name: this.name,
      slot: this.slot,
      target: this.target.name,
      price: this.price,
      deps: this.deps.map(dep => dep.name),
      source: this.source.name
    };
  }
  purchase(): boolean {
    if (this.checkAvailable()) {
      this.bought = true;
    }
    return this.bought;
  }
  getValue(rate: number): number {
    // NB: This could possible introduce negatives.
    return (
      this.getCost() *
      (1 / (this.target.getTotalRate() * (this.bonusFn() - 1)) + 1 / rate)
    );
  }
  isFulfilled(): boolean {
    return this.bought;
  }

  checkAvailable(): boolean {
    if (this.bought) {
      this.available = false;
    } else {
      this.available = this.deps.every((dep: Dependency) => dep.isFulfilled());
    }
    return this.available;
  }

  getCost(): number {
    if (!this.bought) {
      return this.price;
    } else {
      return 0;
    }
  }

  /**
   * Calculates the upgrade's multiplier and returns it.
   * @returns {number} Upgrade Bonus Multiplier
   */
  bonus(): number {
    if (this.bought) {
      return this.bonusFn();
    } else {
      return 1;
    }
  }
}

/**
 * Basic Upgrade is the simple multiplier upgrade.
 */
export class BasicUpgrade extends Upgrade {
  public upgradeType: 'multiplier' = 'multiplier';
  constructor(
    protected target: ParticleGenerator,
    private mult: number,
    protected price: number,
    protected deps: Dependency[]
  ) {
    super(
      `${target.name}: x${mult} Multiplier`,
      target,
      () => mult,
      price,
      deps
    );
  }
  toJSON(): MultiplierJSON {
    return Object.assign({}, super.toJSON(), {
      type: 'multiplier' as 'multiplier',
      target: this.target.name,
      mult: this.mult,
      price: this.price,
      deps: this.deps.map(gen => gen.name)
    });
  }
}

/**
 * Boost Upgrade multiplier is 1+mult*current/num, for instance, +10% for every 10 generators.
 */
export class BoostUpgrade extends Upgrade {
  public upgradeType: 'boost' = 'boost';
  constructor(
    protected target: ParticleGenerator,
    private mult: number,
    private num: number,
    protected price: number,
    protected deps: Dependency[]
  ) {
    super(
      `${target.name}: ${mult}/${num} Boost`,
      target,
      () => 1 + Math.floor(target.num / num) * mult,
      price,
      deps
    );
  }
  toJSON(): BoostJSON {
    return Object.assign({}, super.toJSON(), {
      type: 'boost' as 'boost',
      target: this.target.name,
      mult: this.mult,
      num: this.num,
      price: this.price,
      deps: this.deps.map(gen => gen.name)
    });
  }
}

/**
 * Synergy Upgrade multiplier is 1+source number/100. For instance, boosting II by 1% of IV.
 */
export class SynergyUpgrade extends Upgrade {
  public upgradeType: 'synergy' = 'synergy';
  constructor(
    protected target: ParticleGenerator,
    protected price: number,
    protected deps: Dependency[],
    protected source: ParticleGenerator
  ) {
    super(
      `${target.name}: ${source.name} Synergy`,
      target,
      () => 1 + source.num / 100,
      price,
      deps,
      source
    );
  }
  toJSON(): SynergyJSON {
    return Object.assign({}, super.toJSON(), {
      type: 'synergy' as 'synergy',
      target: this.target.name,
      source: this.source.name,
      price: this.price,
      deps: this.deps.map(gen => gen.name)
    });
  }
}
