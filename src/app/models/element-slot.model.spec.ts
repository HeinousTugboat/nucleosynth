import { ElementData, HydrogenData } from './element-data.model';
import { ElementSlot } from './element-slot.model';
import { ParticleGenerator } from './particle-generator.model';
import { Resource } from './resource.model';
// tslint:disable:no-non-null-assertion

describe('Element Slot', () => {
  const elementData = new ElementData(HydrogenData);
  const testResource = elementData.main as Resource;
  let slot: ElementSlot;
  let gen: ParticleGenerator;

  beforeEach(() => {
    slot = new ElementSlot(elementData);
    gen = slot.generators.get('I') as ParticleGenerator;
  });

  it('should correctly purchase a generator if funds available', () => {
    testResource.quantity = 1110;

    let result: boolean = slot.purchase(gen, 1);
    expect(result).toBeTruthy();
    expect(testResource.quantity).toBe(1100);
    expect(gen.num).toBe(1);

    gen = slot.generators.get('II')!;
    result = slot.purchase(gen, 1);
    expect(result).toBeTruthy();
    expect(testResource.quantity).toBe(1000);
    expect(gen.num).toBe(1);

    gen = slot.generators.get('III')!;
    result = slot.purchase(gen, 1);
    expect(result).toBeTruthy();
    expect(testResource.quantity).toBe(0);
    expect(gen.num).toBe(1);
  });

  describe('production functions', () => {
    it('should calculate the generator production: I 1, x2, x3 => 6/sec', () => {
      gen['_num'] = 1;
      gen.upgrades[0].bought = true;
      gen.upgrades[1].bought = true;
      slot.updateRate();
      testResource.quantity = 0;
      const result = slot.tick(2000);
      expect(result).toBe(12);
      expect(slot['production']).toBe(6);
    });

    it('should calculate the tier production: I 10, x2, x3 => 60/sec', () => {
      gen['_num'] = 10;
      gen.upgrades[0].bought = true;
      gen.upgrades[1].bought = true;
      slot.updateRate();
      testResource.quantity = 0;
      const result = slot.tick(2000);
      expect(result).toBe(120);
      expect(slot['production']).toBe(60);
    });

    it('should calculate the element production: I 1, II 1 @ 10, III 1 @ 80 => 91/sec', () => {
      /* I have no idea why this is like this.. but it's how the original was tested */
      gen['_num'] = 1;
      gen = slot.generators.get('II')!;
      gen['_num'] = 1;
      gen.power = 10;
      gen = slot.generators.get('III')!;
      gen['_num'] = 1;
      gen.power = 80;

      slot.updateRate();
      testResource.quantity = 0;
      const result = slot.tick(2000);
      expect(result).toEqual(182);
      expect(slot['production']).toBe(91);
    });
  });


  describe('visibility functions', () => {
    it('should show visible generators', () => {
      gen['_num'] = 1;

      const visible = slot.visibleGenerators();
      expect(visible.length).toBe(2);
      expect(visible[0]).toBe(gen);
    });
  });
});
