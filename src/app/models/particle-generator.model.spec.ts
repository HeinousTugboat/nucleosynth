import { ParticleGenerator } from './particle-generator.model';
import { Resource } from './resource.model';

describe('Particle Generators', () => {
  const testResource = Resource.get('tH');
  let generator: ParticleGenerator;

  beforeEach(() => {
    generator = new ParticleGenerator('Test Gen', 10, 1, 1.12, testResource);
  });

  describe('purchase', () => {
    it('should return the price of a generator 1: I 5 -> 6', () => {
      generator['_num'] = 5;
      expect(generator.getCost()).toBe(18);
    });
    it('should return the price of a generator 2: VI 10 -> 11', () => {
      generator = new ParticleGenerator('Test Gen', 1e6, 1, 1.14, testResource);
      generator['_num'] = 10;
      expect(generator.getCost()).toBe(3_707_222);
    });
    it('should return the price of a generator 3: I 1 -> 11', () => {
      generator['_num'] = 1;
      expect(generator.getCost(10)).toBe(197);
      // expect(generator.getCost(10)).toEqual(202);
    });
    it('should return the price of a generator 4: I 10 -> 110', () => {
      generator['_num'] = 10;
      expect(generator.getCost(100)).toBe(21_617_032);
      // expect(generator.getCost(100)).toEqual(21_617_082);
    });
    it('should return the price of a generator 5: Max, 100 H, I 10', () => {
      generator['_num'] = 10;

      testResource.quantity = 100;
      const max = generator.getCostMax();
      expect(max.n).toBe(2);
      expect(max.cost).toBe(66);
    });

    it('should purchase as many generators as requested', () => {
      generator['_num'] = 5;
      testResource.quantity = 65;
      const result = generator.purchase(3);

      expect(result).toBeTruthy();
      expect(testResource.quantity).toBe(5);
      // expect(testResource.quantity).toBe(4);
      expect(generator.num).toBe(8);
    });

    it('should purchase as many generators as possible', () => {
      generator['_num'] = 5;
      testResource.quantity = 45;
      const result = generator.purchaseMax();

      expect(result).toBeTruthy();
      expect(testResource.quantity).toBe(7);
      expect(generator.num).toBe(7);
    });

    it('should not purchase generator if cost is not met', () => {
      generator['_num'] = 5;
      testResource.quantity = 10;
      const result = generator.purchase();

      expect(result).toBeFalsy();
      expect(testResource.quantity).toBe(10);
      expect(generator.num).toBe(5);
    });
  });
});
