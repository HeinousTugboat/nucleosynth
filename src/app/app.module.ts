import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GeneratorTableComponent } from '@components/generator-table/generator-table.component';
import { SidebarComponent } from '@components/sidebar/sidebar.component';
import { GeneratorViewComponent } from '@containers/generator-view/generator-view.component';
import { UpgradeViewComponent } from '@containers/upgrade-view/upgrade-view.component';
import { EntriesPipe } from '@pipes/entries.pipe';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    GeneratorViewComponent,
    UpgradeViewComponent,
    SidebarComponent,
    GeneratorTableComponent,
    EntriesPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
