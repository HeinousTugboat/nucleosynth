import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatCardModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreLoopService } from '@services/core-loop.service';
import { DataService } from '@services/data.service';
import { GeneratorService } from '@services/generator.service';
import { SaveService } from '@services/save.service';
import { StateService } from '@services/state.service';
import { AppComponent } from './app.component';

class MockCoreLoopService { }
class MockStateService { }
class MockDataService { }
class MockGeneratorService { }
class MockSaveService { }

@Component({ selector: 'app-sidebar', template: '' })
class SidebarStubComponent { }

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: CoreLoopService, useClass: MockCoreLoopService },
        { provide: StateService, useClass: MockStateService },
        { provide: DataService, useClass: MockDataService },
        { provide: GeneratorService, useClass: MockGeneratorService },
        { provide: SaveService, useClass: MockSaveService }
      ],
      declarations: [
        AppComponent,
        SidebarStubComponent
      ],
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule

      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'NucleoSynth' and version '0.0.0'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Nucleosynth');
    expect(app.version).toEqual('0.0.0');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.name-card h1').textContent).toContain('Nucleosynth');
    expect(compiled.querySelector('.name-card span').textContent).toContain('0.0.0');
  }));
});
