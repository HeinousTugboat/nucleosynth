import { Pipe, PipeTransform } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ElementData, HydrogenData } from '@models/element-data.model';
import { ElementSlot } from '@models/element-slot.model';
import { GeneratorTableComponent } from './generator-table.component';

@Pipe({ name: 'entries' })
export class EntriesMockPipe implements PipeTransform {
  transform(map: Map<any, any>): any[] {
    return [];
  }
}

describe('GeneratorTableComponent', () => {
  let component: GeneratorTableComponent;
  let fixture: ComponentFixture<GeneratorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GeneratorTableComponent,
        EntriesMockPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorTableComponent);
    component = fixture.componentInstance;
    component.slot = new ElementSlot(new ElementData(HydrogenData));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
