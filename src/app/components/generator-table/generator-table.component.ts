import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ElementSlot } from '@models/element-slot.model';

@Component({
  selector: 'app-generator-table',
  templateUrl: './generator-table.component.html',
  styleUrls: ['./generator-table.component.scss']
})
export class GeneratorTableComponent implements OnInit {

  dataColumns = ['genName', 'genProd', 'genNumber', 'genTotalProd', 'genPrice', 'genBuy'];

  @Input()
  slot!: ElementSlot;

  constructor() { }

  ngOnInit() {
  }

}
