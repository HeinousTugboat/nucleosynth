import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ElementData, HydrogenData, ElementJSON } from '@models/element-data.model';
import { ElementSlot } from '@models/element-slot.model';
import { ParticleGenerator } from '@models/particle-generator.model';
import { SidebarComponent } from './sidebar.component';
import { StateService } from '@services/state.service';
import { Resource } from '@models/resource.model';
import { MatButtonModule, MatCardModule, MatSidenavModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

const carbonJSON: ElementJSON = {
  name: 'Carbon',
  symbol: 'C',
  isotopes: {},
  ionization_energy: [],
  electron_affinity: [],
  number: 6,
  abundance: 0.18,
  van_der_wals: 1.7e-10,
  electronegativity: 2.55,
  exotic: 'xC',
  main: '12C',
  anions: [],
  cations: [],
  negative_factor: 70.79457843841377,
  positive_factor: 26.915348039269166
};

const oxygenJSON: ElementJSON = {
  name: 'Oxygen',
  symbol: 'O',
  isotopes: {},
  ionization_energy: [],
  electron_affinity: [],
  number: 8,
  abundance: 0.65,
  van_der_wals: 1.52e-10,
  electronegativity: 3.44,
  exotic: 'xO',
  main: '16O',
  anions: [],
  cations: [],
  negative_factor: 549.5408738576248,
  positive_factor: 3.4673685045253166
};

const hydrogen = new ElementData(HydrogenData);
const carbon = new ElementData(carbonJSON);
const oxygen = new ElementData(oxygenJSON);

class MockStateService {
  unlocked: Map<ElementData, boolean> = new Map([[hydrogen, true], [carbon, false], [oxygen, true]]);
  resources: Resource[] = [];
}

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: StateService, useClass: MockStateService }
      ],
      declarations: [SidebarComponent],
      imports: [
        NoopAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('visibility functions', () => {
    it('should show visible elements', inject([StateService], (state: StateService) => {
      state.resources = [
        Resource.get('1H'), Resource.get('8C')
      ];

      state.unlocked = new Map([[hydrogen, true], [carbon, true], [oxygen, false]]);

      const elements = component.visibleElements();
      expect(elements).toContain(hydrogen);
      expect(elements).toContain(carbon);
      expect(elements).not.toContain(oxygen);
      expect(elements.length).toBe(2);
    }));

    it('should show visible resources for element', inject([StateService], (state: StateService) => {
      state.resources = [Resource.get('1H')];

      Resource.get('1H').quantity = 1;
      Resource.get('16O').quantity = 0;
      Resource.get('eV').quantity = 0;
      Resource.list.delete('2H');

      // TODO: Add Statistics check in here..
      const resources = component.visibleResources(hydrogen);
      expect(resources).toContain(Resource.get('1H'));
      expect(resources.length).toBe(1);
    }));

    xit('shold show misc resources');

    it('should filter visible resources', inject([StateService], (state: StateService) => {
      state.slots = [
        new ElementSlot(hydrogen),
        new ElementSlot(carbon)
      ];

      const elements = component.activeElements();
      expect(elements).toContain(hydrogen);
      expect(elements).toContain(carbon);
      expect(elements).not.toContain(oxygen);
      expect(elements.length).toBe(2);

    }));
  });
});
