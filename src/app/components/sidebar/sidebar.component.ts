import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { CoreLoopService } from '@services/core-loop.service';
import { DataService } from '@services/data.service';
import { Resource } from '@models/resource.model';
import { StateService } from '@services/state.service';
import { ElementData } from '@models/element-data.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class SidebarComponent implements OnInit {

  elements: any[] = [];
  columnsToDisplay: string[] = ['name', 'value'];
  expandedElement = '';

  constructor(
    public coreLoop: CoreLoopService,
    public stateService: StateService,
    private data: DataService,
    // private format: FormatService,
    // private util: UtilService,
    // private visibility: VisibilityService
  ) { }

  ngOnInit() {
  }

  visibleResources(element: ElementData): Resource[] {
    const resources: Resource[] = [];

    for (const resource of this.stateService.resources) {
      if (element.resources.includes(resource) && resource.quantity > 0) {
        resources.push(resource);
      }
    }

    return resources;
  }

  visibleElements(): ElementData[] {
    const elements: ElementData[] = [];

    for (const [element, visible] of this.stateService.unlocked.entries()) {
      if (visible) { elements.push(element); }
    }

    return elements;
  }

  activeElements(): ElementData[] {
    return this.stateService.slots.map(slot => slot.element);
  }

  // activeElements(player: Player): string[] {
  //   const result: string[] = [];
  //   for (const slot of player.elementSlots) {
  //     if (slot) {
  //       result.push(slot.element);
  //     }
  //   }
  //   result.push('');
  //   return result;
  // }

  // visibleResources(element: string, player: Player) {
  //   return this.visibility.visible(this.data.resources, this.isResourceVisible.bind(this), element, null, player);
  // }

  // isResourceVisible(name: string, currentElement: string, player: Player) {
  //   if (player.resources[name] === null) {
  //     return false;
  //   }

  //   // This is for global resources e.g. protons, which do not
  //   // belong to any element
  //   const elements = this.data.resources[name].elements;
  //   if (Object.keys(elements).length === 0 && currentElement === '') {
  //     return true;
  //   }

  // for (const element in elements) {
  //   if (currentElement === element &&
  //     (player.statistics.exoticRun[element] &&
  //       typeof player.statistics.exoticRun[element][name] !== 'undefined' ||
  //       this.data.elements[element].exotic === name)) {
  //     return true;
  //   }
  // }

  //   return false;
  // }

}
