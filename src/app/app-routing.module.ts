import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneratorViewComponent } from '@containers/generator-view/generator-view.component';
import { UpgradeViewComponent } from '@containers/upgrade-view/upgrade-view.component';
// import { ExoticViewComponent } from '@containers/exotic-view/exotic-view.component';
// import { ElementsComponent } from '@components/elements/elements.component';
// import { AchievementsComponent } from '@components/achievements/achievements.component';
// import { DashboardComponent } from '@components/dashboard/dashboard.component';
// import { OptionsComponent } from '@components/options/options.component';
// import { StatisticsComponent } from '@components/statistics/statistics.component';
// import { DarkViewComponent } from '@containers/dark-view/dark-view.component';
// import { ReactionViewComponent } from '@containers/reaction-view/reaction-view.component';
// import { RedoxViewComponent } from '@containers/redox-view/redox-view.component';
// import { FusionViewComponent } from '@containers/fusion-view/fusion-view.component';

const routes: Routes = [
  { path: '', component: GeneratorViewComponent },
  { path: 'generators', component: GeneratorViewComponent },
  { path: 'upgrades', component: UpgradeViewComponent },
  // { path: 'exotic', component: ExoticViewComponent },
  // { path: 'elements', component: ElementsComponent },
  // { path: 'achievements', component: AchievementsComponent },
  // { path: 'dashboard', component: DashboardComponent },
  // { path: 'options', component: OptionsComponent },
  // { path: 'statistics', component: StatisticsComponent },
  // { path: 'dark', component: DarkViewComponent },
  // { path: 'reactions', component: ReactionViewComponent },
  // { path: 'redox', component: RedoxViewComponent },
  // { path: 'fusion', component: FusionViewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
