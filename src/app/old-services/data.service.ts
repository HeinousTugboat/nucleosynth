import { Injectable } from '@angular/core';
import { Player, PlayerGlobalUpgrades, ElementSlot } from '../old-models/player';
import { GeneratorData } from '../old-models/generator';
import { ResourceTree } from '../old-models/resource';
import { ElementData } from '../old-models/element';
// import { AchievementData } from './models/achievement';
import { UpgradeData, GlobalUpgradeData } from '../old-models/upgrade';
import { ConstantData } from '../old-models/constant';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _elements: ElementData = {};
  private _startPlayer: Player = new Player;
  private _resources: ResourceTree = {};
  private _generators: GeneratorData = {};
  // private _achievements: AchievementData;
  private _upgrades: UpgradeData = {};
  private _exoticUpgrades: UpgradeData = {};
  private _darkUpgrades: UpgradeData = {};
  private _constants: ConstantData = {} as ConstantData;
  // private _elementSlot: ElementSlot = {};
  private _globalUpgrades: GlobalUpgradeData = {};

  private _html: any;
  private _radioisotopes: any[] = [];
  private _loaded = false;

  // TODO: DataService info built by script in original
  constructor(private http: HttpClient) {
    const label = (l: string) => map((x) => ({ [l]: x }));

    forkJoin([
      this.http.get('assets/constants.json').pipe(label('constants')),
      // this.http.get('assets/start_player.json').pipe(label('startPlayer')),
      this.http.get('assets/elements.json').pipe(label('elements')),
      this.http.get('assets/resources.json').pipe(label('resources')),
      this.http.get('assets/generators.json').pipe(label('generators')),
      this.http.get('assets/upgrades.json').pipe(label('upgrades')),
      this.http.get('assets/exotic_upgrades.json').pipe(label('exoticUpgrades')),
      this.http.get('assets/dark_upgrades.json').pipe(label('darkUpgrades')),
      this.http.get('assets/global_upgrades.json').pipe(label('global_upgrades')),
      this.http.get('assets/achievements.json').pipe(label('achievements')),
      // this.http.get('assets/element_slot.json').pipe(label('elementSlot'))
    ]).subscribe((dataArray) => {
      const dataTree = dataArray.reduce((acc, val) => ({ ...acc, ...val }));
      console.log(dataTree);
      this._constants = dataTree.constants as ConstantData;
      this._generators = dataTree.generators as GeneratorData;
      this._elements = dataTree.elements as ElementData;
      // this._startPlayer = dataTree.startPlayer as Player;
      this._resources = dataTree.resources as ResourceTree;
      // this._achievements = dataTree.achievements as AchievementData;
      this._upgrades = dataTree.upgrades as UpgradeData;
      this._exoticUpgrades = dataTree.exoticUpgrades as UpgradeData;
      this._darkUpgrades = dataTree.darkUpgrades as UpgradeData;
      this._globalUpgrades = dataTree.global_upgrades as GlobalUpgradeData;
      // this._elementSlot = dataTree.elementSlot as ElementSlot;
      this._loaded = true;
    }, (err) => console.warn('Data Service went Kablooey! ', err));
  }

  get elements(): Readonly<ElementData> { return this._elements; }
  get startPlayer(): Readonly<Player> { return this._startPlayer; }
  get resources(): Readonly<ResourceTree> { return this._resources; }
  get generators(): Readonly<GeneratorData> { return this._generators; }
  // get achievements(): Readonly<AchievementData> { return this._achievements; }
  get upgrades(): Readonly<UpgradeData> { return this._upgrades; }
  get exoticUpgrades(): Readonly<UpgradeData> { return this._exoticUpgrades; }
  get darkUpgrades(): Readonly<UpgradeData> { return this._darkUpgrades; }
  get constants(): Readonly<ConstantData> { return this._constants; }
  // get elementSlot(): Readonly<ElementSlot> { return this._elementSlot; }
  get globalUpgrades(): Readonly<GlobalUpgradeData> { return this._globalUpgrades; }

  get html(): Readonly<any> { return this._html; }
  get radioisotopes(): Readonly<any> { return this._radioisotopes; }
  get loaded(): boolean { return this._loaded; }

}
