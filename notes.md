# Notes

Notes to keep track of which unit tests I've started copying over from nucleogenesis.

## Unit Tests

* Achievements
* Dark
* ~~Dashboard~~
* Element
* Exotic
* ~~Generators~~
* ~~MainLoop~~
* Mechanics
* Options
* Reactions
* Redox
* ServiceFormat
* ServiceReaction
* ~~ServiceState~~
* ServiceUtil
* ServiceVisibility
* Upgrades
